// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VModal from 'vue-js-modal'
// import LoadScript from 'vue-plugin-load-script';

Vue.config.productionTip = false;
export const EventBus = new Vue();
Vue.use(VModal);
// Vue.use(LoadScript);

// Vue.loadScript("../uncompress/uncompress.js")
//   .then(() => {
//     console.log("boo?");
//     // Script is loaded, do something
//   })
//   .catch(() => {
//     console.log("fail");
//     // Failed to fetch script
//   });
// Vue.component('v-icon', Icon);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
})
